# Hapi study

## Prepare docker environment
With this step you can work into a separately docker container that does not change the status of your machine.

* vscode web get started with docker

```
docker pull registry.gitlab.com/nicolalandro/vscode_docker
docker run -it --rm -p 8080:8080 registry.gitlab.com/nicolalandro/vscode_docker

firefox localhost:8080
```
* into vscode web install Browser preview plugin and into a terminal install chromium and nodejs
```
apt update
apt install chromium

curl -sL https://deb.nodesource.com/setup_16.x -o /tmp/nodesource_setup.sh
bash /tmp/nodesource_setup.sh
apt install nodejs
node -v
```

* if you need some key for git you can:
```
docker ps
# finde <COTAINER-ID>
docker cp ~/ssh./<ID-RSA-FILE> <COTAINER-ID>:/root/
GIT_SSH_COMMAND='ssh -i /root/<ID-RSA-FILE>' git push
```

## hapi basics
* create project
```
npm init @hapipal hapi-pal-tutorial
```
* install requirements and run server
```
npm install
npm start
```
* create route
```
npx hpal make route pinger
# go into lib/routes/routes and edit method, route and options handler
```
* linting
```
npm run lint
# or if you want to apply it
npm run lint -- --fix
```
* database
```
git fetch pal --tags
git cherry-pick objection
```
* create model
```
npx hpal make model Riddles
# edit lib/models/Riddles.js
npx knex migrate:make add-riddles
# edit migration created into lib/migrations/xxx.js
npx knex migrate:latest
```
* testing simplified with swagger
```
git cherry-pick swagger 
npm install
```

## How to use running server:
* ping
```
curl localhost:3000/pinger
```
* first routes
```
curl http://localhost:3000/riddle-random

curl http://localhost:3000/riddle-answer/no-body
```
* add a slug
```
curl -H "Content-Type: application/json" -X POST -d '{"slug": "see-saw", "question": "We see it once in a year, twice in a week, but never in a day. What is it?", "answer": "The letter E"}' http://localhost:3000/riddle
```

# References
* docker
* vscode
* nodejs
* [hapi-pal](https://hapipal.com/): official website
* [introducig hapi-pal](https://medium.com/@hapipal/introducing-hapi-pal-550c13f30c5b): introduction
* [hapi-pal get started](https://hapipal.com/getting-started#getting-started)
