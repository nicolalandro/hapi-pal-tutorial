'use strict';

module.exports = {
    method: 'get',
    path: '/pinger',
    options: {
        handler: (request, h) => {

            return { ping: 'pong' };
        }
    }
};
